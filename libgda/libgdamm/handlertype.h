// -*- c++ -*-
// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!
#ifndef _LIBGDAMM_HANDLERTYPE_H
#define _LIBGDAMM_HANDLERTYPE_H


#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>

// -*- C++ -*- //

/* handlertype.h
 *
 * Copyright 2006 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or(at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/object.h>
#include <libgdamm/datahandler.h>
#include <libgda/handlers/gda-handler-type.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct _GdaHandlerType GdaHandlerType;
typedef struct _GdaHandlerTypeClass GdaHandlerTypeClass;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */


#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Gnome
{

namespace Gda
{ class HandlerType_Class; } // namespace Gda

} // namespace Gnome
#endif //DOXYGEN_SHOULD_SKIP_THIS

namespace Gnome
{

namespace Gda
{

/** Default handler for GType values.
 *
 * @ingroup DataHandlers
 */

class HandlerType : public Glib::Object,
                    public DataHandler
{
  
#ifndef DOXYGEN_SHOULD_SKIP_THIS

public:
  typedef HandlerType CppObjectType;
  typedef HandlerType_Class CppClassType;
  typedef GdaHandlerType BaseObjectType;
  typedef GdaHandlerTypeClass BaseClassType;

  // noncopyable
  HandlerType(const HandlerType&) = delete;
  HandlerType& operator=(const HandlerType&) = delete;

private:  friend class HandlerType_Class;
  static CppClassType handlertype_class_;

protected:
  explicit HandlerType(const Glib::ConstructParams& construct_params);
  explicit HandlerType(GdaHandlerType* castitem);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

public:

  HandlerType(HandlerType&& src) noexcept;
  HandlerType& operator=(HandlerType&& src) noexcept;

  virtual ~HandlerType() noexcept;

  /** Get the GType for this class, for use with the underlying GObject type system.
   */
  static GType get_type()      G_GNUC_CONST;

#ifndef DOXYGEN_SHOULD_SKIP_THIS


  static GType get_base_type() G_GNUC_CONST;
#endif

  ///Provides access to the underlying C GObject.
  GdaHandlerType*       gobj()       { return reinterpret_cast<GdaHandlerType*>(gobject_); }

  ///Provides access to the underlying C GObject.
  const GdaHandlerType* gobj() const { return reinterpret_cast<GdaHandlerType*>(gobject_); }

  ///Provides access to the underlying C instance. The caller is responsible for unrefing it. Use when directly setting fields in structs.
  GdaHandlerType* gobj_copy();

private:

  
protected:

 HandlerType();

public:
  
  static Glib::RefPtr<HandlerType> create();


public:

public:
  //C++ methods used to invoke GTK+ virtual functions:

protected:
  //GTK+ Virtual Functions (override these to change behaviour):

  //Default Signal Handlers::


};

} // namespace Gda
} // namespace Gnome


namespace Glib
{
  /** A Glib::wrap() method for this object.
   * 
   * @param object The C instance.
   * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
   * @result A C++ instance that wraps this C instance.
   *
   * @relates Gnome::Gda::HandlerType
   */
  Glib::RefPtr<Gnome::Gda::HandlerType> wrap(GdaHandlerType* object, bool take_copy = false);
}


#endif /* _LIBGDAMM_HANDLERTYPE_H */

