// -*- c++ -*-
// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!
#ifndef _LIBGDAMM_DATACOMPARATOR_H
#define _LIBGDAMM_DATACOMPARATOR_H


#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>

// -*- C++ -*- //

/* datacomparator.h
 *
 * Copyright 2006 libgdamm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or(at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <libgdamm/datamodel.h>
#include <libgda/gda-data-comparator.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct _GdaDataComparator GdaDataComparator;
typedef struct _GdaDataComparatorClass GdaDataComparatorClass;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */


#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace Gnome
{

namespace Gda
{ class DataComparator_Class; } // namespace Gda

} // namespace Gnome
#endif //DOXYGEN_SHOULD_SKIP_THIS

namespace Gnome
{

namespace Gda
{

//TODO: Document and/or wrap this:
typedef GdaDiff Diff;
	
/** Simple data model's contents comparison.
 *
 * The DataComparator is a simple object which takes two DataModel objects
 * and compare them. Actual comparison is performed when the 
 * compute_diff() is called; for each difference found, the diff-computed signal
 * is emitted (any user installed signal handler which returns FALSE stops 
 * the computing process).
 *
 * After the differences have been computed, they can each be accessed using 
 * gda_data_comparator_get_diff(), the number of differences found being 
 * returned by gda_data_comparator_get_n_diffs().
 * There are some limitations to this object:
 * <ul>
 *   <li>The data models compared must have the same number and type of columns</li>
 *   <li>The comparison is done column-for-column: one cannot omit columns in the comparison,
 * nor compare columns with different positions</li>
 * </ul>
 *
 * @ingroup DataHandlers
 */

class DataComparator
 : public Glib::Object,
   public DataModel
{
  
#ifndef DOXYGEN_SHOULD_SKIP_THIS

public:
  typedef DataComparator CppObjectType;
  typedef DataComparator_Class CppClassType;
  typedef GdaDataComparator BaseObjectType;
  typedef GdaDataComparatorClass BaseClassType;

  // noncopyable
  DataComparator(const DataComparator&) = delete;
  DataComparator& operator=(const DataComparator&) = delete;

private:  friend class DataComparator_Class;
  static CppClassType datacomparator_class_;

protected:
  explicit DataComparator(const Glib::ConstructParams& construct_params);
  explicit DataComparator(GdaDataComparator* castitem);

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

public:

  DataComparator(DataComparator&& src) noexcept;
  DataComparator& operator=(DataComparator&& src) noexcept;

  virtual ~DataComparator() noexcept;

  /** Get the GType for this class, for use with the underlying GObject type system.
   */
  static GType get_type()      G_GNUC_CONST;

#ifndef DOXYGEN_SHOULD_SKIP_THIS


  static GType get_base_type() G_GNUC_CONST;
#endif

  ///Provides access to the underlying C GObject.
  GdaDataComparator*       gobj()       { return reinterpret_cast<GdaDataComparator*>(gobject_); }

  ///Provides access to the underlying C GObject.
  const GdaDataComparator* gobj() const { return reinterpret_cast<GdaDataComparator*>(gobject_); }

  ///Provides access to the underlying C instance. The caller is responsible for unrefing it. Use when directly setting fields in structs.
  GdaDataComparator* gobj_copy();

private:

  
protected:
    explicit DataComparator(const Glib::RefPtr<DataModel>& old_model, const Glib::RefPtr<DataModel>& new_model);


public:
  
  static Glib::RefPtr<DataComparator> create(const Glib::RefPtr<DataModel>& old_model, const Glib::RefPtr<DataModel>& new_model);


  //TODO: The nb_cols parmetetr is unnecessary:
  //TODO: Documentation
  void set_key_for_columns(const std::vector<int>& col_numbers);
  

  /** Actually computes the differences between the data models for which @a comp is defined. 
   * 
   * For each difference computed, stored in a Gda::Diff structure, the "diff-computed" signal is emitted.
   * If one connects to this signal and returns <tt>false</tt> in the signal handler, then computing differences will be
   * stopped and an error will be returned.
   * 
   * @return <tt>true</tt> if all the differences have been sucessfully computed, and <tt>false</tt> if an error occurred.
   */
  bool compute_diff();
  
  /** Get the number of differences as computed by the last time compute_diff() was called.
   * 
   * @return The number of computed differences.
   */
  int get_n_diffs() const;
  
  /** Get a pointer to the Gda::Diff structure representing the difference which number is @a pos
   * 
   * @param pos The requested difference number (starting at 0).
   * @return A pointer to a Gda::Diff, or <tt>0</tt> if @a pos is invalid.
   */
  const Diff* get_diff(int pos);
		
  /** 
   *
   * @return A PropertyProxy that allows you to get or set the value of the property,
   * or receive notification when the value of the property changes.
   */
  Glib::PropertyProxy< Glib::RefPtr<DataModel> > property_old_model() ;

/** 
   *
   * @return A PropertyProxy_ReadOnly that allows you to get the value of the property,
   * or receive notification when the value of the property changes.
   */
  Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> > property_old_model() const;

  /** 
   *
   * @return A PropertyProxy that allows you to get or set the value of the property,
   * or receive notification when the value of the property changes.
   */
  Glib::PropertyProxy< Glib::RefPtr<DataModel> > property_new_model() ;

/** 
   *
   * @return A PropertyProxy_ReadOnly that allows you to get the value of the property,
   * or receive notification when the value of the property changes.
   */
  Glib::PropertyProxy_ReadOnly< Glib::RefPtr<DataModel> > property_new_model() const;


//TODO: Register the type in libgda properly?
 

  /**
   * @par Slot Prototype:
   * <tt>bool on_my_%diff_computed(Diff* diff)</tt>
   *
   */

  Glib::SignalProxy1< bool,Diff* > signal_diff_computed();


public:

public:
  //C++ methods used to invoke GTK+ virtual functions:

protected:
  //GTK+ Virtual Functions (override these to change behaviour):

  //Default Signal Handlers::


};

} // namespace Gda
} // namespace Gnome


namespace Glib
{
  /** A Glib::wrap() method for this object.
   * 
   * @param object The C instance.
   * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
   * @result A C++ instance that wraps this C instance.
   *
   * @relates Gnome::Gda::DataComparator
   */
  Glib::RefPtr<Gnome::Gda::DataComparator> wrap(GdaDataComparator* object, bool take_copy = false);
}


#endif /* _LIBGDAMM_DATACOMPARATOR_H */

