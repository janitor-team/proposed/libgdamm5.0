// -*- c++ -*-
// Generated by gmmproc 2.46.1 -- DO NOT MODIFY!
#ifndef _LIBGDAMM_SQLEXPR_H
#define _LIBGDAMM_SQLEXPR_H


#include <glibmm/ustring.h>
#include <sigc++/sigc++.h>

// -*- C++ -*- //

/* quarklist.h
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <glibmm/object.h>
#include <libgda/sql-parser/gda-statement-struct-parts.h>

#ifndef DOXYGEN_SHOULD_SKIP_THIS
extern "C" { typedef struct _GdaSqlExpr GdaSqlExpr; }
#endif

namespace Gnome
{

namespace Gda
{

//TODO: The C API seems to expect direct struct access as public API, 
//but do apps ever need to use that API?
/** This contains any expression, either as a value, a variable, or as other 
 * types of expressions. 
 */
class SqlExpr
{
  public:
#ifndef DOXYGEN_SHOULD_SKIP_THIS
  typedef SqlExpr CppObjectType;
  typedef GdaSqlExpr BaseObjectType;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

  /** Get the GType for this class, for use with the underlying GObject type system.
   */
  static GType get_type() G_GNUC_CONST;

  SqlExpr();

  explicit SqlExpr(GdaSqlExpr* gobject, bool make_a_copy = true);

  SqlExpr(const SqlExpr& other);
  SqlExpr& operator=(const SqlExpr& other);

  SqlExpr(SqlExpr&& other) noexcept;
  SqlExpr& operator=(SqlExpr&& other) noexcept;

  ~SqlExpr() noexcept;

  void swap(SqlExpr& other) noexcept;

  ///Provides access to the underlying C instance.
  GdaSqlExpr*       gobj()       { return gobject_; }

  ///Provides access to the underlying C instance.
  const GdaSqlExpr* gobj() const { return gobject_; }

  ///Provides access to the underlying C instance. The caller is responsible for freeing it. Use when directly setting fields in structs.
  GdaSqlExpr* gobj_copy() const;

protected:
  GdaSqlExpr* gobject_;

private:

  
public:
  //TODO: Wrap GdaSqlAnyPart.
  explicit SqlExpr(GdaSqlAnyPart *parent);
  

  bool empty() const;

  
  /** Creates a new string representing a field. You need to free the returned string
   * using Glib::free();
   * 
   * @return A new string with the name of the field or "null" in case @a expr is invalid.
   */
  Glib::ustring serialize() const;

  //TODO: _WRAP_METHOD(void take_select(GdaSqlStatement *stmt), gda_sql_expr_take_select)


};

} // namespace Gda

} // namespace Gnome


namespace Gnome
{

namespace Gda
{

/** @relates Gnome::Gda::SqlExpr
 * @param lhs The left-hand side
 * @param rhs The right-hand side
 */
inline void swap(SqlExpr& lhs, SqlExpr& rhs) noexcept
  { lhs.swap(rhs); }

} // namespace Gda

} // namespace Gnome

namespace Glib
{

/** A Glib::wrap() method for this object.
 * 
 * @param object The C instance.
 * @param take_copy False if the result should take ownership of the C instance. True if it should take a new copy or ref.
 * @result A C++ instance that wraps this C instance.
 *
 * @relates Gnome::Gda::SqlExpr
 */
Gnome::Gda::SqlExpr wrap(GdaSqlExpr* object, bool take_copy = false);

#ifndef DOXYGEN_SHOULD_SKIP_THIS
template <>
class Value<Gnome::Gda::SqlExpr> : public Glib::Value_Boxed<Gnome::Gda::SqlExpr>
{};
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

} // namespace Glib


#endif /* _LIBGDAMM_SQLEXPR_H */

